import os, subprocess, re, glob

DB_URI = os.environ['SQLALCHEMY_DATABASE_URI']
SCHEMA_FILES = os.environ['DB_SCHEMA_FILES']
NEW_CLASSES_DEST_FILE = r'source/models/__init__.py'

TERMINATOR_COMMENT = os.environ['DB_SCHEMA_AUTOGEN_TERMINATOR']

CLASS_MATCHER = re.compile(r'class ([\S]+)\([\S ,.]+\):[\s\S]*?(?=\n+\S|$)')


def replacer_matcher_for(classname):
    return re.compile(r'class ' + classname + r'\([\S ,.]+\):[\s\S]+?(?=\n\S|$| *?' + TERMINATOR_COMMENT + r')')


APPEND_CODE = '\n\n    query : db.Query\n\n'

# create dictionary with classes based on flask-sqlacodegen output
sqlacodegen = subprocess.run('/bin/flask-sqlacodegen --flask %s' % DB_URI, shell=True, stdout=subprocess.PIPE)
if sqlacodegen.returncode is not os.EX_OK:
    exit(1)
generated_code = sqlacodegen.stdout.decode('utf-8')
classes = {c.group(1): c.group(0) + APPEND_CODE for c in CLASS_MATCHER.finditer(generated_code)}

print(classes.keys())

# distribute classes over files
for file_name in glob.glob(SCHEMA_FILES):
    if os.path.isfile(file_name):
        print('- in file ' + file_name)

        # read file
        with open(file_name, mode='r') as file:
            file_content = file.read()

        # replace classes
        any_change = False
        for class_name, code in [x for x in classes.items()]:
            matcher = replacer_matcher_for(class_name)
            if matcher.search(file_content) is not None:
                print(' replacing class ' + class_name)
                file_content = matcher.sub(code, file_content, 1)
                classes.pop(class_name)
                any_change = True

        # write over
        if any_change:
            with open(file_name, mode='w') as file:
                file.write(file_content)
                print(' write complete ' + os.path.join(os.getcwd(), file_name))
                print('--------')

# add remaining classes
if len(classes) > 0:
    with open(NEW_CLASSES_DEST_FILE, mode='a+') as file:
        print('- in file ' + NEW_CLASSES_DEST_FILE)
        for class_name, code in classes.items():
            print(' adding new class for ' + class_name)
            file.write('\n\n' + code)
