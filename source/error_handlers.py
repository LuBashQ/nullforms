from typing import Tuple

from flask import Response, render_template, blueprints

bp = blueprints.Blueprint('error_handlers', __name__)


# noinspection PyUnusedLocal
@bp.app_errorhandler(403)
def page_not_found(e) -> Tuple[Response, int]:
    return render_template('error_pages/403.html'), 403


# noinspection PyUnusedLocal
@bp.app_errorhandler(404)
def page_not_found(e) -> Tuple[Response, int]:
    return render_template('error_pages/404.html'), 404


# noinspection PyUnusedLocal
@bp.app_errorhandler(500)
def page_not_found(e) -> Tuple[Response, int]:
    return render_template('error_pages/500.html'), 500
