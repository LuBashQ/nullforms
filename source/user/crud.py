from typing import Union

import flask
from flask import Response, request, redirect, url_for, render_template
from flask_login import login_required, current_user, logout_user

from models import db, User


@login_required
def user_home() -> str:
    """
    Shows the user's profile page
    :return: the profile page
    """
    return render_template('user/crud/user_details.html')


def register() -> Union[Response, str]:
    """
    View and relative endpoint to create a new account on the website
    """
    from flask_login import current_user, login_user

    if current_user.is_authenticated:
        return flask.redirect('/')

    if request.method == 'POST':
        email = request.form['email']
        passwd = request.form['password']
        username = request.form['username']
        full_name = request.form['full_name']

        if User.query.filter_by(email=email).first() is not None:
            flask.flash('User with given email already exists')
        elif invalid_password(passwd):
            pass
        else:
            # noinspection PyArgumentList
            user = User(email=email, username=username, full_name=full_name)

            user.set_password(passwd)

            db.session.add(user)
            db.session.commit()

            login_user(user)

            return flask.redirect(request.args.get('next') or url_for('homepage.page'))

        return flask.render_template('user/auth/register.html', email=email, username=username, full_name=full_name)
    else:
        return flask.render_template('user/auth/register.html')


def invalid_password(password: str) -> bool:
    """
    Checks if a password respects minimal requirements, and calls flask.flash to give the reason the password is invalid.
    :param password: the plain text password to check
    :return: true if the password is not ok, false otherwise
    """
    if len(password) < 8:
        flask.flash('Password needs to be at least 8 characters long')
        return True
    return False


@login_required
def update() -> Response:
    """
    Updates the user's details. I doesn't distinguish between modified data and not because updating one field has the same
    performance impact of updating the entire row

    :return: A redirection to the user's profile page after the update
    """
    current_user.full_name = request.form.get('fullname')
    current_user.email = request.form.get('email')
    current_user.username = request.form.get('username')
    db.session.commit()
    return redirect(url_for('user.user_home'))


@login_required
def update_password() -> Union[str, Response]:
    """
    Updates che user's password.
    :return: A redirection to the user's profile page if the password has been correctly changed, otherwise the password

    update form with an error message.
    """
    if request.method == 'GET':
        return render_template('user/crud/update_password.html')
    else:
        current = request.form.get('current')
        new = request.form.get('new')
        confirm = request.form.get('new_confirm')

        if new != confirm:
            flask.flash("Passwords don't match")
        elif invalid_password(new):
            pass
        elif not current_user.check_password(current):
            flask.flash('Incorrect password')
        else:
            current_user.set_password(new)
            db.session.commit()
            return redirect(url_for('user.user_home'))

        return render_template('user/crud/update_password.html')


@login_required
def delete() -> Union[str, Response]:
    """
    Allows the user to delete itself. All their surveys and submissions will be deleted due to cascading deletion.

    :return: A redirection to log out in order to allow flask to remove the current_user instance if the deletion was
    successful, a view of the deletion form with an error message otherwise
    """
    if request.method == 'POST':
        if request.form.get('confirmation') is None:
            flask.flash('You need to confirm your deletion!')
            return render_template('user/crud/delete_account.html')
        db.session.delete(current_user)
        db.session.commit()
        logout_user()
        return redirect(url_for('homepage.page'))
    else:
        return render_template('user/crud/delete_account.html')
