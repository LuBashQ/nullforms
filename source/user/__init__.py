from flask import blueprints

from user import auth, crud, submission_list

bp = blueprints.Blueprint('user', __name__)

bp.add_url_rule('/login', methods=['GET', 'POST'],
                view_func=auth.login)
bp.add_url_rule('/logout', methods=['POST'],
                view_func=auth.logout)

bp.add_url_rule('/', methods=['GET'],
                view_func=crud.user_home)
bp.add_url_rule('/register', methods=['GET', 'POST'],
                view_func=crud.register)
bp.add_url_rule('/update', methods=['POST'],
                view_func=crud.update)
bp.add_url_rule('/update_password', methods=['GET', 'POST'],
                view_func=crud.update_password)
bp.add_url_rule('/delete', methods=['GET', 'POST'],
                view_func=crud.delete)

bp.add_url_rule('/submissions', methods=['GET'],
                view_func=submission_list.submissions)
