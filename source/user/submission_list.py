from flask import Response, render_template
from flask_login import login_required, current_user

from models import db, Submission, Survey


@login_required
def submissions() -> Response:
    """ A view that lists the user's submissions """

    return render_template('user/submissions.html',
                           submissions=db.session
                           .query(Submission.time, Survey.title, Submission.id, Survey.id)
                           .join(Survey)
                           .filter(Submission.user == current_user)
                           .order_by(Submission.time.desc()).all())
