from typing import Union

import flask
import flask_login
from flask import Response, url_for

from models import User

login_manager = flask_login.LoginManager()

login_manager.login_view = 'user.login'


def init_login_manager(app):
    login_manager.init_app(app)


@login_manager.unauthorized_handler
def _unauthorized() -> Response:
    flask.flash('You are not logged in')
    return flask.redirect(url_for('user.login', next=flask.request.path))


@login_manager.user_loader
def _load_user(user_id) -> User:
    return User.query.get(user_id)


def login() -> Union[Response, str]:
    from flask_login import current_user, login_user
    from flask import request

    if current_user.is_authenticated:
        return flask.redirect('/')

    if request.method == 'POST':
        email = request.form['email']
        user = User.query.filter_by(email=email).one_or_none()
        passwd = request.form['password']
        if user and user.check_password(passwd):
            login_user(user)
            return flask.redirect(request.args.get('next') or url_for('homepage.page'))
        else:
            flask.flash('Invalid email or password')
            return flask.render_template('user/auth/login.html', email=email)
    else:
        return flask.render_template('user/auth/login.html')


def logout() -> Response:
    from flask_login import current_user, logout_user
    from flask import request

    if current_user.is_authenticated:
        logout_user()

    return flask.redirect(request.args.get('next') or '/')
