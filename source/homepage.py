import flask
from flask import blueprints, Response
from flask_login import current_user, login_required

from models import db, Survey

bp = blueprints.Blueprint('homepage', __name__)


@bp.route('/')
def page() -> Response:
    """
    Shows all the user-created surveys when the user is logged in;
    shows a welcome page otherwise.

    :return: the page where all surveys will be shown
    """
    if current_user.is_authenticated:
        response = flask.render_template('user/surveys_list.html',
                                         surveys=current_user.surveys_created_or_contributed)
        db.session.commit()
        return response
    else:
        return flask.render_template('welcome.html')


@bp.route('/create', methods=['POST'])
@login_required
def create() -> Response:
    """
    Creates a new empty Survey, and redirects to the survey edit page.
    """
    new_survey = Survey(creator=current_user, title='Untitled')
    db.session.add(new_survey)
    db.session.commit()
    return flask.redirect(flask.url_for('surveys.edit', survey_id=new_survey.id))
