from functools import wraps
from typing import Callable, List

import flask
import flask_login


def can_view_results(func) -> Callable:
    """
    Checks if a user can see the surveys results

    :param func: the function that this decorator wraps
    :return: the decorated function
    """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        survey = flask.request.view_args['survey']
        if not survey.user_can_view_results():
            flask.abort(403)
        return func(*args, **kwargs)

    return decorated_view


def can_view_submission(func) -> Callable:
    """
        Checks if a user can see their submissions

        :param func: the function that this decorator wraps
        :return: the decorated function
        """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        survey = flask.request.view_args['survey']
        submission = flask.request.view_args['submission']
        if not survey.user_can_view_results() and submission.user != flask_login.current_user:
            flask.abort(403)
        return func(*args, **kwargs)

    return decorated_view


def can_submit(func) -> Callable:
    """
        Checks if a user can submit their answers

        :param func: the function that this decorator wraps
        :return: the decorated function
        """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        survey = flask.request.view_args['survey']
        if not survey.is_open:
            return flask.render_template('surveys/submission_error.html',
                                         reason='The survey is closed. Contact the creator if you think this was a '
                                                'mistake.')
        if not survey.anonymous and not flask_login.current_user.is_authenticated:
            return flask.current_app.login_manager.unauthorized()

        from models import Submission
        user_submissions: List[Submission] = [x for x in flask_login.current_user.submissions if
                                              x.survey_id == survey.id]

        if user_submissions and survey.unique_responses:
            return flask.render_template('surveys/submission_error.html',
                                         reason='You have already answered to this survey.')

        return func(*args, **kwargs)

    return decorated_view


def can_edit(func) -> Callable:
    """
        Checks if a user can edit a survey

        :param func: the function that this decorator wraps
        :return: the decorated function
        """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        survey = flask.request.view_args['survey']
        if not survey.user_can_edit():
            flask.abort(403)
        return func(*args, **kwargs)

    return decorated_view


def has_ownership(func) -> Callable:
    """
        Checks if a user is the creator of a survey

        :param func: the function that this decorator wraps
        :return: the decorated function
        """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        survey = flask.request.view_args['survey']
        if not survey.user_is_creator():
            flask.abort(403)
        return func(*args, **kwargs)

    return decorated_view
