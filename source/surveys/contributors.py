from typing import Tuple

from flask import request

import surveys.verify
from models import Survey, Contributor, db, User


@surveys.verify.has_ownership
def remove_contributor(survey: Survey, contributor_id: int) -> Tuple[str, int]:
    """
    Removes a contributor

    :param survey: the survey the contributors contributes in
    :param contributor_id: the contributor's id
    :return: an HTTP 204 response
    """
    contributor: Contributor = Contributor.query.filter_by(survey=survey, user_id=contributor_id).one()
    db.session.delete(contributor)
    db.session.commit()

    return '', 204


@surveys.verify.has_ownership
def add_contributor(survey: Survey) -> Tuple[str, int]:
    """
    Adds a contributor

    :param survey: the survey the contributor will contribute in
    :return: an HTTP 204 response
    """
    user_email = request.form['email']
    user = User.query.filter_by(email=user_email).one()
    survey.contributors.append(user)
    db.session.commit()

    return '', 204
