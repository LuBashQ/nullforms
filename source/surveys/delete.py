from typing import Tuple
from flask import Response

import surveys.verify
from models import db


@surveys.verify.has_ownership
def delete(survey) -> Tuple[Response, int]:
    """
    Deletes a survey

    :param survey: the survey to be deleted
    :return: an HTTP 204 response
    """
    db.session.delete(survey)
    db.session.commit()
    return '', 204
