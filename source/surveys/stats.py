from typing import List

from flask import render_template

import surveys.verify
from models import Survey, SubmissionAnswer


@surveys.verify.can_view_results
def show_stats(survey: Survey) -> str:
    submissions = survey.submissions
    answers_to_survey: List[SubmissionAnswer] = SubmissionAnswer.query.filter_by(survey_id=survey.id).all()
    data = list()
    submission_answers = dict()

    for question in survey.survey_questions:
        data.append({'key': question.short_name,
                     'count': len([ans.content for ans in answers_to_survey if ans.question_id == question.id]),
                     'id': question.id})

    for question in survey.survey_questions:
        submission_answers[question.id] = question.answers()

    return render_template('stats/base.html',
                           passed_data=data,
                           submission_number=submissions.__len__(),
                           question_data=submission_answers)
