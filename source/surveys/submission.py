import flask
import flask_login

import surveys.verify
from models import db, Survey, Submission


@surveys.verify.can_submit
def respond(survey: Survey) -> flask.Response:
    """
    Shows the answering form to a survey

    :param survey: the survey to be answered
    :return: the corresponding HTML page
    """
    all_children = [q.depended_on_by_json for q in survey.survey_questions]
    return flask.render_template('surveys/compilation.html', survey=survey, dependant_questions=all_children)


@surveys.verify.can_submit
def submit(survey: Survey) -> flask.Response:
    """
    Submits all answered questions

    :param survey: the survey of which the data will be submitted
    :return: a submission successful HTML page
    """
    submission = Submission(survey=survey)
    if not survey.anonymous:
        submission.user = flask_login.current_user

    db.session.add(submission)

    for question in survey.survey_questions:
        question.generate_form_answers(submission)

    db.session.commit()

    return flask.render_template('surveys/submission_successful.html', survey=survey)
