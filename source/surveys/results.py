from typing import BinaryIO

import flask
from flask import Response

import surveys.verify
from models import Submission, Survey


@surveys.verify.can_view_results
def response_table(survey: Survey) -> str:
    """ This is a view to quickly see a table with all the submitted responses to a survey """
    return flask.render_template('surveys/responses_list.html', survey=survey)


def generate_csv(survey: Survey) -> BinaryIO:
    """
    Function to generate a csv file in memory, populating it with the responses to a survey

    :param survey: the survey to get the responses from
    :return: a binary file containing the data
    """
    import io
    from models.submission import Submission

    with io.StringIO() as csv_buffer:
        import csv

        writer = csv.writer(csv_buffer, delimiter=',')

        writer.writerow(['id'] +
                        (['submitter'] if not survey.anonymous else []) +
                        [q.short_name for q in survey.questions_in_order])
        s: Submission
        for s in Submission.query.filter_by(survey_id=survey.id).order_by(Submission.id).all():
            writer.writerow([s.id] +
                            ([] if survey.anonymous else [s.user.username] if s.user else ['']) +
                            [survey.answer_contents_map.get((s.id, q.id), '') for q in
                             survey.questions_in_order])

        file_obj = io.BytesIO()
        file_obj.write(csv_buffer.getvalue().encode())
        file_obj.seek(0)

    return file_obj


@surveys.verify.can_view_results
def download(survey: Survey) -> Response:
    """ An endpoint to download the responses to a survey in csv format """
    filename = '%s.csv' % survey.title.replace(' ', '_').strip()
    return flask.send_file(generate_csv(survey), mimetype='text/csv', as_attachment=True, attachment_filename=filename)


# noinspection PyUnusedLocal
@surveys.verify.can_view_submission
def view_submission(survey: Survey, submission: Submission) -> str:
    """ Shows a read-only view of the selected submission by filling the survey with the provided answers """
    ans_map = {x.question_id: x for x in submission.submission_answers}
    return flask.render_template('surveys/view_submission.html', submission=submission, map=ans_map)
