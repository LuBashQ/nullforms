from datetime import datetime, timedelta
from typing import Tuple

import flask
from flask import request, url_for, render_template

import surveys.verify
from models import db, SurveyQuestion, Survey


def is_boolean(field: dict) -> bool:
    """
    Check if a field is boolean

    :param field: take a dictionary of a field, which contains the key 'type.
        the structure of field should be like this:
        field = {
                "label": label,
                "name": name,
                "id": id,
                "type": type
        }
    :return: true if the field is boolean, false if not
    """
    return field["type"] == "boolean"


def process_datetime(date_field: str, time_field: str):
    """
    Given a date string(%Y-%m-%d and a time string %H:%M it combines them together to obtain a datetime object
    :param date_field: string with format %Y-%m-%d
    :param time_field: string with format %H:%M
    :return: datetime object
    """
    year, month, day = map(int, date_field.split('-'))
    time = list(map(int, time_field.split(':')))
    date_object = datetime(year, month, day, time[0], time[1], 0)
    return date_object


def set_values_in_survey(fields: dict, new_survey: Survey) -> None:
    """
    Given a dictionary of fields, in which are saved the fields' values,
    and an object Survey, it sets the fields in the survey object with the
    corresponding values in the dictionary

    :param fields: dictionary of structure:
                    fields={
                            "field1":{
                                    "name": name,
                                    "id": id,
                                    "label": label,
                                    "type": type,
                                    "value": value
                                    }
                            "field2":{...}
                            ...
                            }
    :param new_survey: Survey to update
    :return: it updates the object new_survey
    """

    for key in fields:
        if fields[key]["type"] == "boolean":
            setattr(new_survey, key, False)
        elif fields[key]["type"] == "datetime":
            date_field = request.form[key + "_date"]
            time_field = request.form[key + "_time"]
            time_field = ':'.join(time_field.split(':', 2)[:2])

            if date_field == "" or time_field == "":
                setattr(new_survey, key, None)
                continue

            datetime_string = date_field + " " + time_field
            datetime_obj = datetime.strptime(datetime_string, '%Y-%m-%d %H:%M')
            new_datetime = datetime_obj + timedelta(minutes=int(request.form["timezone"]))

            datetime_object = process_datetime(str(new_datetime.date()), str(new_datetime.time()))
            setattr(new_survey, key, datetime_object)
        if key in request.form:
            if fields[key]["type"] == "boolean":
                if key == "is_open":
                    new_survey.is_open = True
                else:
                    setattr(new_survey, key, True)
            else:
                setattr(new_survey, key, request.form[key])


def get_values_from_survey(survey_fields: dict, survey: Survey) -> None:
    """
    Given a dictionary of fields and an Object Survey,
    it copies the values form the dictionary into the object

    :param survey_fields:  dictionary of structure:
                    fields={
                            "field1":{
                                    "name": name,
                                    "id": id,
                                    "label": label,
                                    "type": type,
                                    "value": value
                                    }
                            "field2":{...}
                            ...
                            }
    :param survey: survey from which the values are taken
    :return: it updates the dictionary fields with the values.
    """
    for field in survey_fields:
        if is_boolean(survey_fields[field]):
            if field == "is_open":
                survey_fields[field]["value"] = survey.is_open
            else:
                survey_fields[field]["value"] = survey.__getattribute__(field)
        else:
            survey_fields[field]["value"] = survey.__getattribute__(field)
    survey_fields["start_timestamp"]["value"] = {
        "date": survey.start_timestamp.date() if survey.start_timestamp else None,
        "time": survey.start_timestamp.time() if survey.start_timestamp else None}
    survey_fields["end_timestamp"]["value"] = {
        "date": survey.end_timestamp.date() if survey.end_timestamp else None,
        "time": survey.end_timestamp.time() if survey.end_timestamp else None}


def get_questions_from_list(questions_list: list, questions: dict) -> None:
    """
    It updates the dictionary questions with all the questions in questions_list,
    resulting in a dictionary of the structure:
    questions_list = {
                    0: {
                        "question_order": int,
                        "schema": string,
                        "text": string,
                        "choices": list
                        }
                    }

    :param questions_list:
    :param questions:
    :return:
    """
    for q in questions_list:
        content = q.question_content
        questions[q.order] = {
            "question_order": q.order,
            "schema": q.question_type,
            "text": content["text"],

        }
        if "choices" in content:
            questions[q.order]["choices"] = content["choices"]


def append_questions_from_dict_to_survey(questions: dict, survey: Survey) -> None:
    """
    It adds the questions to the survey.

    :param questions: dict of questions with structure:
                        questions = {
                                    0: {
                                        "field1": field1,
                                        "field2": field2,
                                        ...
                                        }
                                    1: {
                                        "field1": field1,
                                        "field2": field2,
                                        ...
                                        }
                                        ...
                                    }
    :param survey: object survey to which the questions are added
    :return: add the questions to the survey
    """
    for q in questions:
        new_q = SurveyQuestion()
        for field in questions[q]:
            new_q.__setattr__(field, questions[q][field])
        survey.survey_questions.append(new_q)


@surveys.verify.can_edit
def edit(survey):
    """
    Visualizes the edit page (GET) or it submits the new edited values (POST)

    :param survey: the survey
    :return: the edit page if the request was GET, a redirection to the homepage otherwise
    """
    survey_fields = Survey.get_fields_edit()

    if request.method == 'GET':
        # update the values in survey_fields
        get_values_from_survey(survey_fields, survey)

        return render_template("surveys/edit/page.html", survey=survey, survey_fields=survey_fields,
                               schemas=SurveyQuestion.__subclasses__())
    else:
        # survey fields
        set_values_in_survey(survey_fields, survey)
        db.session.commit()

        return flask.redirect(url_for('surveys.edit', survey_id=survey.id))


@surveys.verify.can_edit
def toggle_open(survey: Survey) -> Tuple[str, int]:
    """
    Opens or closes a survey based on it's status

    :param survey: the survey
    :return: an HTTP 204 response
    """
    survey.is_open = 'open' in request.form.keys()
    db.session.commit()
    return '', 204


# noinspection PyUnusedLocal
@surveys.verify.can_edit
def edit_q(question: SurveyQuestion, survey: Survey) -> Tuple[str, int]:
    """
    Edits polymorfically a question based on its effettive type

    :param question: the question to be modified
    :param survey: the survey the question belongs to
    :return: an HTTP 204 response
    """
    question.edit()
    db.session.commit()
    return '', 204


# noinspection PyUnusedLocal
@surveys.verify.can_edit
def delete_q(question: SurveyQuestion, survey: Survey) -> Tuple[str, int]:
    """
    Deletes a question from its survey

    :param question: the question
    :param survey: the survey the question belongs to
    :return: an HTTP 204 response
    """
    db.session.delete(question)
    db.session.commit()
    return '', 204


@surveys.verify.can_edit
def add_q(survey: Survey) -> str:
    """
        Adds polymorfically a question to a survey

        :param survey: the survey the question will belong to
        :return: the generated HTML code of the newly created question
        """
    q_class: type(SurveyQuestion) = next(q for q in SurveyQuestion.__subclasses__() if
                                         q.__mapper_args__['polymorphic_identity'] == request.form['question_type'])
    # noinspection PyArgumentList
    new_q = q_class(survey=survey, order=request.form['order'], question_content={})
    db.session.add(new_q)
    db.session.commit()
    return render_template('questions/question_block.html', q=new_q)


@surveys.verify.can_edit
def set_order(survey: Survey):
    """
    Orders the question in a top-down fashion, from the top of the page to the bottom
    :param survey: the survey to be ordered
    :return: an HTTP 204 response
    """
    order = {
        int(qid): pos
        for pos, qid
        in enumerate(request.form['order'].split(','), start=1)}

    for q in survey.survey_questions:
        q.order = order[q.id]

    db.session.commit()

    return '', 204
