import flask

from models import Submission, Survey, SurveyQuestion

from . import submission, results, stats, edit, contributors, delete

bp = flask.blueprints.Blueprint('surveys', __name__)


# noinspection PyUnusedLocal
@bp.url_value_preprocessor
def get_survey(endpoint, values: dict):
    survey_id = values.pop('survey_id', None)
    submission_id = values.pop('submission_id', None)
    question_id = values.pop('question_id', None)

    if survey_id:
        s = Survey.query.get(survey_id)
        if not s: flask.abort(404)
        values['survey'] = s

    if submission_id and survey_id:
        s = Submission.query.filter_by(survey_id=survey_id, id=submission_id).one_or_none()
        if not s: flask.abort(404)
        values['submission'] = s

    if question_id and survey_id:
        q = SurveyQuestion.query.filter_by(survey_id=survey_id, id=question_id).one_or_none()
        if not q: flask.abort(404)
        values['question'] = q


bp.add_url_rule('/', methods=['GET'],
                view_func=submission.respond)
bp.add_url_rule('/submit', methods=['POST'],
                view_func=submission.submit)

bp.add_url_rule('/view_results', methods=['GET'],
                view_func=results.response_table)
bp.add_url_rule('/download', methods=['GET'],
                view_func=results.download)
bp.add_url_rule('/view_sub/<submission_id>', methods=['GET'],
                view_func=results.view_submission)

bp.add_url_rule('/stats', methods=['GET'],
                view_func=stats.show_stats)

bp.add_url_rule('/edit', methods=['GET', 'POST'],
                view_func=edit.edit)
bp.add_url_rule('/set_open', methods=['POST'],
                view_func=edit.toggle_open)
bp.add_url_rule('/edit_q/<question_id>', methods=['POST'],
                view_func=edit.edit_q)
bp.add_url_rule('/delete_q/<question_id>', methods=['DELETE'],
                view_func=edit.delete_q)
bp.add_url_rule('/add_q', methods=['POST'],
                view_func=edit.add_q)
bp.add_url_rule('/set_order', methods=['POST'],
                view_func=edit.set_order)

bp.add_url_rule('/remove_contributor/<contributor_id>', methods=['DELETE'],
                view_func=contributors.remove_contributor)
bp.add_url_rule('/add_contributor', methods=['POST'],
                view_func=contributors.add_contributor)
bp.add_url_rule('/delete', methods=['DELETE'],
                view_func=delete.delete)
