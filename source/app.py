import os

from flask import Flask
from dotenv import load_dotenv

import error_handlers
import homepage
import models
import surveys
import user

load_dotenv()
app = Flask(__name__, template_folder='../templates', static_folder='../static')

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_URI']
# app.config['SQLALCHEMY_ECHO'] = True

app.secret_key = os.environ['NULLFORMS_SECRET_KEY']

models.init_db(app)

user.auth.init_login_manager(app)

app.register_blueprint(homepage.bp, url_prefix='/')
app.register_blueprint(surveys.bp, url_prefix='/s/<survey_id>')
app.register_blueprint(user.bp, url_prefix='/user')
app.register_blueprint(error_handlers.bp)

if __name__ == '__main__':
    app.run()
