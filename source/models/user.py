from functools import cached_property
from typing import List

import flask_login
from sqlalchemy import desc
from sqlalchemy.orm import relationship

from . import db, types


class User(db.Model, flask_login.UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    email = db.Column(db.String, nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)
    username = db.Column(db.String, nullable=False)
    full_name = db.Column(types.StringNullIsEmpty)

    query: db.Query

    # END sqlacodegen
    from .survey import Survey
    from .contributor import Contributor

    surveys_contributed = relationship(
        "Survey",
        secondary=Contributor.__table__,
        back_populates="contributors",
        order_by=desc(Survey.creation_timestamp))

    surveys: List[Survey]
    from .submission import Submission
    submissions: List[Submission]

    def set_password(self, password: str) -> None:
        from werkzeug.security import generate_password_hash
        self.password = generate_password_hash(password)

    def check_password(self, password: str) -> bool:
        from werkzeug.security import check_password_hash
        return check_password_hash(self.password, password)

    @cached_property
    def surveys_created_or_contributed(self) -> List[Survey]:
        from .survey import Survey
        from .contributor import Contributor

        created = Survey.query.filter(Survey.creator == self)
        contributed = Survey.query.join(Contributor).filter(Contributor.user == self)
        return created.union(contributed).order_by(desc(Survey.creation_timestamp)).all()
