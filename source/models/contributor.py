from models import db


class Contributor(db.Model):
    __tablename__ = 'contributors'

    user_id = db.Column(db.ForeignKey('users.id', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True,
                        nullable=False)
    survey_id = db.Column(db.ForeignKey('surveys.id', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True,
                          nullable=False)
    added = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    survey = db.relationship('Survey', primaryjoin='Contributor.survey_id == Survey.id')
    user = db.relationship('User', primaryjoin='Contributor.user_id == User.id')

    query: db.Query

    # END sqlacodegen
    __table__: db.Table
