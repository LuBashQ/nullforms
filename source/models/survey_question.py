from abc import abstractmethod
from typing import List, Optional, Dict

from sqlalchemy import and_
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.orm import remote, foreign, backref

from models import db, types


class SurveyQuestion(db.Model):
    __tablename__ = 'survey_questions'
    __table_args__ = (
        db.ForeignKeyConstraint(('survey_id', 'dependency_question_id'),
                                ('survey_questions.survey_id', 'survey_questions.id'), ondelete='RESTRICT',
                                onupdate='CASCADE'),
    )

    id = db.Column(db.Integer, primary_key=True, nullable=False, server_default=db.FetchedValue())
    survey_id = db.Column(db.ForeignKey('surveys.id', ondelete='CASCADE', onupdate='RESTRICT'), primary_key=True,
                          nullable=False)
    order = db.Column(db.Integer, nullable=False)
    question_content = db.Column(MutableDict.as_mutable(db.JSON), nullable=False)
    question_type = db.Column(db.Enum('open', 'close_multiple', 'close_single', name='question_schema_type'),
                              nullable=False, server_default=db.FetchedValue())
    custom_name = db.Column(types.StringNullIsEmpty())
    dependency_question_id = db.Column(db.Integer)
    dependency_answer_condition = db.Column(db.JSON(none_as_null=True))

    survey = db.relationship('Survey', primaryjoin='SurveyQuestion.survey_id == Survey.id',
                             backref=backref('survey_questions', passive_deletes='all'))

    query: db.Query

    # END sqlacodegen
    from .submission_answer import SubmissionAnswer

    depends_on = db.relationship('SurveyQuestion',
                                 primaryjoin=and_(survey_id == remote(survey_id),
                                                  foreign(dependency_question_id) == remote(id)),
                                 backref='depended_on_by')
    depended_on_by: List

    @property
    def depended_on_by_json(self):
        return [{'id': x.id, 'condition': x.dependency_answer_condition} for x in self.depended_on_by]

    __mapper_args__ = {
        'polymorphic_on': question_type,
        'polymorphic_identity': None
    }

    submission_answers: List[SubmissionAnswer]

    all_types: List[str] = question_type.type.enums

    @property
    def submission_path(self):
        return 'questions/compilation/' + self.question_type + '.html'

    @property
    def result_path(self):
        return 'questions/answers/' + self.question_type + '.html'

    @property
    def edit_path(self):
        return 'questions/edit/' + self.question_type + '.html'

    @property
    def short_name(self):
        if self.custom_name:
            return self.custom_name
        else:
            return 'q_%d' % self.id

    pretty_typename: str = None

    @abstractmethod
    def generate_form_answers(self, submission) -> Optional[SubmissionAnswer]:
        """
        This method creates and adds to the database a SubmissionAnswer based on the form data associated with the current
         HTML request.
        :param submission: the submission to associate the answer to
        :return: the SubmissionAnswer created, None if nothing was created
        """
        raise NotImplementedError

    @abstractmethod
    def answers(self) -> Dict[str, List]:
        raise NotImplementedError

    @abstractmethod
    def edit(self):
        from flask import request
        self.custom_name = request.form['q_name']
        if request.form.get('is_dependent') and request.form.get('dependency') != '':
            self.dependency_question_id = request.form.get('dependency')
            self.dependency_answer_condition = request.form.get('dependency_condition')
        else:
            self.dependency_question_id = None
            self.dependency_answer_condition = None
