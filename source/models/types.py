import sqlalchemy.types

from models import db


# noinspection PyAbstractClass
class StringNullIsEmpty(sqlalchemy.types.TypeDecorator):
    """ Maps NULL in the database to an empty string, and vice-versa """

    impl = db.String

    cache_ok = True

    def process_bind_param(self, value, dialect):
        return value if value != '' else None

    def process_result_value(self, value, dialect):
        return value or ''
