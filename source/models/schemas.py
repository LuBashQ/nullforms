import itertools
from collections import Counter
from typing import List, Dict, Optional

from flask import request

from .submission import Submission
from .submission_answer import SubmissionAnswer
from .survey_question import SurveyQuestion


class Open(SurveyQuestion):
    __mapper_args__ = {
        'polymorphic_identity': 'open'
    }

    pretty_typename: str = 'Open'

    def generate_form_answers(self, submission: Submission) -> Optional[SubmissionAnswer]:
        ans = request.form.get(f'answer_{self.id}')
        if ans and ans != '':
            return SubmissionAnswer(question=self, content=ans, submission=submission)

    def answers(self) -> Dict[str, List]:
        ans_d = dict()
        contents = [a.content for a in self.submission_answers]

        counter = dict(Counter(contents))

        ans_d['list'] = [{'key': key, 'count': counter[key]} for key in sorted(counter, reverse=True,
                                                                               key=lambda k: counter[k])]

        return ans_d

    def edit(self):
        SurveyQuestion.edit(self)
        self.question_content = {
            'text': request.form['text']
        }


class CloseSingle(SurveyQuestion):
    __mapper_args__ = {
        'polymorphic_identity': 'close_single'
    }

    pretty_typename: str = 'Single choice'

    def answer_is_valid(self, ans: str) -> bool:
        return ans in self.question_content['choices']

    def generate_form_answers(self, submission: Submission) -> Optional[SubmissionAnswer]:
        ans = request.form.get(f'answer_{self.id}')
        if ans and self.answer_is_valid(ans):
            return SubmissionAnswer(question=self, content=ans, submission=submission)

    def answers(self) -> Dict[str, List]:
        ans_d = dict()
        contents = [a.content for a in self.submission_answers]

        counter = Counter(contents)

        ans_d['bar'] = [{'key': key, 'count': counter[key]} for key in counter]
        ans_d['cake'] = [{'key': key, 'count': counter[key] / len(contents) * 100.} for key in counter]

        return ans_d

    def edit(self):
        SurveyQuestion.edit(self)
        self.question_content = {
            'text': request.form['text'],
            'choices': request.form.getlist('choices')
        }


class CloseMultiple(SurveyQuestion):
    __mapper_args__ = {
        'polymorphic_identity': 'close_multiple'
    }

    pretty_typename: str = 'Multiple choice'

    def answer_is_valid(self, ans: str) -> bool:
        return ans in self.question_content['choices']

    def generate_form_answers(self, submission: Submission) -> Optional[SubmissionAnswer]:
        ans_list: List[str] = request.form.getlist(f'answer_{self.id}')
        if ans_list and all(self.answer_is_valid(a) for a in ans_list):
            return SubmissionAnswer(question=self, content=ans_list, submission=submission)

    def answers(self) -> Dict[str, List]:
        ans_d = dict()

        flattened_content = [a.content for a in self.submission_answers]

        counter = Counter(list(itertools.chain(*flattened_content)))

        ans_d['bar'] = [{'key': key, 'count': counter[key]} for key in counter]

        return ans_d

    def edit(self):
        SurveyQuestion.edit(self)
        self.question_content = {
            'text': request.form['text'],
            'choices': request.form.getlist('choices')
        }
