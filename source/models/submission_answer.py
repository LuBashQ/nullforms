from sqlalchemy.orm import backref

from models import db


class SubmissionAnswer(db.Model):
    __tablename__ = 'submission_answers'
    __table_args__ = (
        db.ForeignKeyConstraint(('question_id', 'survey_id'), ['survey_questions.id', 'survey_questions.survey_id'],
                                ondelete='CASCADE', onupdate='RESTRICT'),
        db.ForeignKeyConstraint(('submission_id', 'survey_id'), ['submissions.id', 'submissions.survey_id'],
                                ondelete='CASCADE', onupdate='RESTRICT')
    )

    submission_id = db.Column(db.Integer, primary_key=True, nullable=False)
    question_id = db.Column(db.Integer, primary_key=True, nullable=False)
    content = db.Column(db.JSON(none_as_null=True))
    survey_id = db.Column(db.Integer, nullable=False)

    question = db.relationship('SurveyQuestion',
                               primaryjoin='and_(foreign(SubmissionAnswer.question_id) == SurveyQuestion.id, '
                                           'SubmissionAnswer.survey_id == SurveyQuestion.survey_id)',
                               backref=backref('submission_answers', passive_deletes='all'))
    submission = db.relationship('Submission',
                                 primaryjoin='and_(SubmissionAnswer.submission_id == Submission.id, '
                                             'SubmissionAnswer.survey_id == Submission.survey_id)',
                                 backref=backref('submission_answers', passive_deletes='all'))

    query: db.Query

    # END sqlacodegen
