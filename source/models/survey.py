from functools import cached_property
from typing import List, Dict, Tuple

from sqlalchemy import desc
from sqlalchemy.orm import backref, relationship

from . import db


class Survey(db.Model):
    __tablename__ = 'surveys'
    __table_args__ = (
        db.CheckConstraint('(end_timestamp >= start_timestamp) OR (end_timestamp IS NULL)'),
    )

    id = db.Column(db.Integer, primary_key=True, server_default=db.FetchedValue())
    creator_id = db.Column(db.ForeignKey('users.id', ondelete='CASCADE', onupdate='RESTRICT'), nullable=False)
    title = db.Column(db.String, nullable=False)
    unique_responses = db.Column(db.Boolean, nullable=False, server_default=db.FetchedValue())
    _is_open = db.Column('open_status', db.Boolean, nullable=False, server_default=db.FetchedValue())
    anonymous = db.Column(db.Boolean, nullable=False, server_default=db.FetchedValue())
    public_results = db.Column(db.Boolean, nullable=False, server_default=db.FetchedValue())
    creation_timestamp = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())
    start_timestamp = db.Column(db.DateTime)
    end_timestamp = db.Column(db.DateTime)

    creator = db.relationship('User', primaryjoin='Survey.creator_id == User.id',
                              backref=backref('surveys', order_by=desc(creation_timestamp),
                                              passive_deletes='all'))

    query: db.Query

    # END sqlacodegen
    from .submission import Submission
    from .survey_question import SurveyQuestion
    from .contributor import Contributor

    contributors = relationship(
        "User",
        secondary=Contributor.__table__,
        back_populates="surveys_contributed")

    survey_questions: List[SurveyQuestion]
    submissions: List[Submission]

    @staticmethod
    def get_fields_edit() -> dict:
        booleans = ["unique_responses", "is_open", "anonymous", "public_results"]
        field_list = {"title": {"label": "Survey\'s title"},
                      "start_timestamp": {"label": "Open date"},
                      "end_timestamp": {"label": "Close date"},
                      "unique_responses": {"label": "Unique response"},
                      "is_open": {"label": "Open"},
                      "anonymous": {"label": "Anonymous"},
                      "public_results": {"label": "Public results"}
                      }
        for item in field_list:
            field_list[item]["name"] = item
            field_list[item]["id"] = item
            field_list[item]["type"] = "boolean" if item in booleans else "string"
        field_list["start_timestamp"]["type"] = "datetime"
        field_list["end_timestamp"]["type"] = "datetime"
        return field_list

    @cached_property
    def questions_in_order(self):
        return sorted(self.survey_questions, key=lambda q: q.order)

    def user_can_edit(self) -> bool:
        import flask_login
        return self.user_is_creator() \
            or (flask_login.current_user.is_authenticated and flask_login.current_user in self.contributors)

    def user_can_view_results(self) -> bool:
        return self.public_results or self.user_can_edit()

    def user_is_creator(self) -> bool:
        import flask_login
        return flask_login.current_user.is_authenticated and self.creator_id == flask_login.current_user.id

    @property
    def is_open(self):
        import datetime

        if self.start_timestamp and self.start_timestamp <= datetime.datetime.utcnow():
            self._is_open = True
            self.start_timestamp = None
        if self.end_timestamp and self.end_timestamp <= datetime.datetime.utcnow():
            self._is_open = False
            self.end_timestamp = None

        return self._is_open

    @is_open.setter
    def is_open(self, value):
        self._is_open = value

    @cached_property
    def answer_contents_map(self) -> Dict[Tuple[int, int], str]:
        from .submission_answer import SubmissionAnswer

        return {(s_id, q_id): cont
                for cont, s_id, q_id in db.session
                    .query(SubmissionAnswer.content, SubmissionAnswer.submission_id, SubmissionAnswer.question_id)
                    .filter_by(survey_id=self.id).all()}
