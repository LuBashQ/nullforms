from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def init_db(app):
    db.app = app
    db.init_app(app)


__all__ = ['db', 'User', 'Contributor', 'Survey', 'SurveyQuestion', 'Submission', 'SubmissionAnswer']

from .user import User
from .contributor import Contributor
from .survey_question import SurveyQuestion
from .survey import Survey
from .submission_answer import SubmissionAnswer
from .submission import Submission
from .schemas import *
