from typing import List

from sqlalchemy.orm import backref

from . import db


class Submission(db.Model):
    __tablename__ = 'submissions'

    id = db.Column(db.Integer, primary_key=True, nullable=False, server_default=db.FetchedValue())
    survey_id = db.Column(db.ForeignKey('surveys.id', ondelete='CASCADE', onupdate='RESTRICT'), primary_key=True,
                          nullable=False)
    user_id = db.Column(db.ForeignKey('users.id', ondelete='SET NULL', onupdate='RESTRICT'))
    time = db.Column(db.DateTime, nullable=False, server_default=db.FetchedValue())

    survey = db.relationship('Survey', primaryjoin='Submission.survey_id == Survey.id',
                             backref=backref('submissions', passive_deletes='all'))
    user = db.relationship('User', primaryjoin='Submission.user_id == User.id', backref='submissions')

    query: db.Query

    # END sqlacodegen
    from .submission_answer import SubmissionAnswer

    submission_answers: List[SubmissionAnswer]
