$(document).ready(() => {
    new Bar_chart(data, ['count'], 'main').draw()
    $('#frequency_table').hide()
    $('#sub').hide()
})

let selector = $('#selector')
let graph = $('#type')

selector.change(() => {
    $('#type').show()
    $('#sub').hide()
    $('#frequency_table').hide()
    $('#data_question_title').hide()
    graph.empty()
    graph.append(new Option('Select the graph type', 'type', true, true))
    document.getElementById('type').options[0].disabled = true
    Object.keys(question_data[selector.val()]).forEach((x) =>
        graph.append(new Option(x, x))
    )
    $('#type[value=type]').attr('disabled', 'disabled').siblings().removeAttr("disabled");
})

graph.change(() => {
    let title = $('#data_question_title')
    title.text(`data for ${selector.find('option:selected').text()}`)
    let type = graph.val()
    let data_to_process = question_data[selector.val()]
    if (type === 'bar')
        new Bar_chart(data_to_process[type], ['count'], 'sub').draw()
    else if (type === 'cake')
        new Pie_chart(data_to_process[type], 'sub').draw()
    else
        new Frequency_list(data_to_process[type], 'graph_per_question').draw()
    title.show()
})

