/**
 * Defines a stacked bar chart with the provided map. The x-axis will correspond to the keys of the map and the y-axis to their
 * corresponding values
 * @constructor
 * @param {Map} data the data that needs to be displayed
 * @param {Array} params the parameters for the stacked bar chart (need to be maximum 2)
 */
class Bar_chart extends Chart {
    constructor(d, params, id,) {
        super(d, id, {top: 30, right: 30, bottom: 30, left: 30}, 450, 450);
        this.params = params
        $('#sub').show()
        $('#frequency_table').hide()
    }

    draw() {
        d3.selectAll(`svg#${this.id} > *`).remove()
        const svg = d3.select(`svg#${this.id}`)
            .attr('width', this.w + this.margin.bottom + this.margin.top)
            .attr('height', this.h + this.margin.bottom + this.margin.top)
            .append('g')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)

        const dataset = d3.layout.stack()(
            this.params.map(k => {
                return this.data.map(v => {
                    return {x: v.key, y: +v[k]}
                })
            }))

        const x = d3.scale.ordinal()
            .domain(dataset[0].map(d => d.x))
            .rangeRoundBands([0, this.w], 0.03)

        const y = d3.scale.linear()
            .domain([0, d3.max(dataset, d => d3.max(d, v => v.y0 + v.y))])
            .range([this.h, 0])

        const colors = ["#ac75c6", "#fde678"];

        const yAxis = d3.svg.axis()
            .scale(y)
            .orient('left')
            .ticks(d3.max(dataset, d => d3.max(d, v => v.y0 + v.y)) + 1)
            .tickSize(-this.w, 0, 0)
            .tickFormat(d => d)

        const xAxis = d3.svg.axis()
            .scale(x)
            .orient('bottom')

        svg.append('g')
            .attr('class', 'n_answers')
            .call(yAxis)

        svg.append('g')
            .attr('class', 'questions')
            .attr('transform', `translate(0, ${this.h})`)
            .call(xAxis)

        const groups = svg.selectAll('g.answers')
            .data(dataset)
            .enter().append('g')
            .attr('class', 'answers')
            .style('fill', (d, i) => colors[i])

        const rects = groups.selectAll('rect')
            .data(d => d)
            .enter()
            .append('rect')
            .attr('x', d => x(d.x))
            .attr('y', this.h)
            .attr('height', 0)
            .attr('width', x.rangeBand())
            .transition()
            .duration(500)
            .attr('y', d => y(d.y0 + d.y))
            .attr('height', d => y(d.y0) - y(d.y0 + d.y))
    }
}
