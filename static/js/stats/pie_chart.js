/**
 * Defines a pie chart
 * @constructor
 * @param {Map} data the data that needs to be displayed
 */
class Pie_chart extends Chart {
    constructor(d, id) {
        super(d, id, {}, 450, 450);
        this.radius = Math.min(this.w, this.h) / 2
        $('#sub').show()
        $('#frequency_table').hide()
    }

    draw() {
        d3.selectAll(`svg#${this.id} > *`).remove()
        const svg = d3.select(`svg#${this.id}`)
            .attr('width', this.w)
            .attr('height', this.h)
            .append('g')
            .attr('transform', `translate(${this.w / 2}, ${this.h / 2})`)

        const colors = [this.data.length];
        {
            const interval = 1. / this.data.length;
            let c = interval / 2;
            for (let i = 0; i < this.data.length; i++) {
                colors[i] = d3.interpolatePlasma(c);
                c += interval;
            }
        }

        const arc = d3.svg.arc()
            .outerRadius(this.radius - 20)
            .innerRadius(this.radius - 100);

        const pie = d3.layout.pie()
            .sort(null)
            .startAngle(0)
            .endAngle(2 * Math.PI)
            .value(function (d) {
                return d.count;
            });

        const g = svg.selectAll(".fan")
            .data(pie(this.data))
            .enter()
            .append("g")
            .attr("class", "fan");

        let angleInterpolation = d3.interpolate(0, 2 * Math.PI);

        g.append("path")
            .transition().duration(500)
            .attrTween("d", d => {
                let originalEnd = d.endAngle;
                return t => {
                    let currentAngle = angleInterpolation(t);
                    if (currentAngle < d.startAngle) {
                        return "";
                    }
                    d.endAngle = Math.min(currentAngle, originalEnd);
                    return arc(d);
                };
            })
            .style("fill", (d, i) => colors[i])

        g.append("text")
            .attr("transform", function (d) {
                return "translate(" + arc.centroid(d) + ")";
            })
            .style("text-anchor", "middle")
            .text(function (d) {
                return `${d.data.key}\n${d.data.count}%`
            });
    }
}
