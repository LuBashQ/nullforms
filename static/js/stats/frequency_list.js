/**
 * This class defines a frequency list from a given sorted map
 * @constructor
 * @param {Map} data the input data
 * @param {string} parent the id of the parent container of this list
 */
class Frequency_list extends Chart {
    constructor(data) {
        super(data);
        $('#sub').show()
        $('#frequency_table').hide()
    }

    draw() {
        let table = document.getElementById('frequency_table').getElementsByTagName('tbody')[0]
        $('#table_body').empty()

        Object.keys(this.data).forEach(k => {
            let row = table.insertRow(k)
            let count = row.insertCell(0)
            let answer = row.insertCell(1)

            answer.innerText = this.data[k].key
            count.innerText = this.data[k].count
        })
        $('#sub').hide()
        $('#frequency_table').show()
    }

}