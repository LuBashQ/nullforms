/**
 * Defines a generic chart
 * @constructor
 * @param {Map} d the data that will be shown
 * @param {Array} params the parameters by which the data will be grouped (for stacked charts etc...)
 * @param {string} elem_id the id of the element that will contain the graph
 * @param {int} width the width of the graph
 * @param {int} height the height of the graph
 *
 */
class Chart {
    constructor(data, id = null, margin = {}, width, height) {
        this.data = data
        this.id = id
        this.margin = margin
        this.w = width
        this.h = height
    }
}