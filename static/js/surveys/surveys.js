function toggle_fail(checkbox, reason) {
    checkbox.checked = !checkbox.checked
    console.error(reason);
    alert('There was a problem opening or closing the survey.');
}

function set_label(checkbox, label) {
    if (checkbox.checked) {
        label.innerHTML = 'Accepting Responses';
    } else {
        label.innerHTML = 'Closed';
    }
    checkbox.disabled = false;
}

/**
 * Toggles the opening and closure of a survey by sending an asynchronous HTTP request
 * @param checkbox the checkbox which will be affected by the result of the request
 * @return {Promise<void>}
 */
function toggle_open(checkbox) {
    const form = checkbox.form;
    const label = form.querySelector('label.label-inline');
    const formData = [...(new FormData(form))];

    checkbox.disabled = true;
    label.innerHTML = '<i class="gg-loadbar"></i>';

    fetch(form.action, {
        method: form.method,
        body: new URLSearchParams(formData)
    }).then(response => {
        if (!response.ok) {
            toggle_fail(checkbox, response);
        }
        set_label(checkbox, label);
    }).catch(reason => {
        toggle_fail(checkbox, reason);
        set_label(checkbox, label);
    });
}

function fix_conditional_box(conditional_div, valid) {
    const then = () => conditional_div.get(0).dispatchEvent(new Event('change'));
    if (valid) {
        conditional_div.fadeIn(then);
    } else {
        conditional_div.fadeOut(then);
    }
    conditional_div.find('input, textarea').prop('disabled', !valid);
    conditional_div.find('input, textarea').prop('readonly', !valid);
}

function update_dependency(box, questions) {
    const visible = $(box).is(':visible');
    questions.forEach(q => {
        let valid = false;
        let conditional_div = $(`div#${q.id}`)

        if (visible) {
            if ($(box).find('input').is('[type=radio]') || $(box).find('input').is('[type=checkbox]')) {
                let checked = [...$(box).find(`input[name=answer_${box.id}]:checked`)]
                checked.forEach(v => {
                    if (q.condition === '' || $(v).val() === q.condition) valid = true;
                });
            } else {
                let text_box = $(box).find(`input[name=answer_${box.id}], textarea[name=answer_${box.id}]`)
                valid = (q.condition === '' && text_box.val() !== '')
                    || (q.condition !== '' && text_box.val() === q.condition);
            }
        }

        fix_conditional_box(conditional_div, valid)

    })
}