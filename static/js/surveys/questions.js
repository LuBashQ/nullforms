/**
 * Sends an empty HTTP DELETE request
 * @param url the url of the deletion method
 * @param success callback that gets called when the request is successful
 * @param fail callback that gets called when the request fails
 * @return {Promise<Response>}
 */
function fetch_delete(url, success, fail) {
    return fetch(url, {
        method: 'DELETE',
        body: new URLSearchParams()
    }).then(response => {
        if (!response.ok) {
            fail(response);
        } else {
            success()
        }
    }).catch(fail)
}

/**
 * Retrieve question block of the correct type and id
 * @param {string} url the url of the fetching method
 * @param {string} type the type of question
 * @return {Promise<string | void>}
 */
function fetch_question_block(url, type) {
    return fetch(url, {
        method: 'POST',
        body: new URLSearchParams({'question_type': type, 'order': $('.question_box').length + 1})
    }).then(response => {
        if (!response.ok) {
            throw Error(response.statusText)
        }
        return response.text()
    }).then(t => {
        // fade in block
        $(t).hide().appendTo('#question_boxes').fadeIn()
            // set up auto submission
            .find('form').formAutoSubmit()
            // focus input
            .find('input').focus();
    }).catch(alert)
}


/**
 * Uses fetch to update the display order of questions
 * @param url the endpoint to send the order to
 * @param order an array of question ids, sorted in the wanted order
 * @returns {Promise<Response> | void}
 * @param {string} url
 * @param {array<int>} order
 */
function fetch_set_order(url, order) {
    const last = $(document).data('lastSubmittedQOrder');
    const order_string = order.toString();

    // do not submit if content is the same as last time
    if (last && last === order_string) return;

    return fetch(url, {
        method: 'POST',
        body: new URLSearchParams({'order': order.toString()})
    }).then(response => {
        if (!response.ok) {
            throw Error(response.statusText)
        }
        $(document).data('lastSubmittedQOrder', order_string);
        return response;
    }).catch(alert)
}

function remove_cyclic(block_array, current) {
    let toRemove = [current];
    while (toRemove.length > 0) {
        const r = toRemove.shift();
        for (let i = 0; i < block_array.length; i++) {
            if (block_array[i].dep === r) {
                toRemove.push(block_array[i].id);
                block_array.splice(i, 1);
                i--;
            }
        }
    }
}

function generate_question_list(select) {
    const $question = $(select.closest('div.question_box'));
    const $select = $(select);

    const val = $select.val();

    $select.empty();
    let option = new Option('Select a question', '');
    $select.append(option);

    let blocks = [];
    $question.siblings('div.question_box').each((i, b) => {
        const $b = $(b);
        const name_box = $b.find('input[name=q_name]');

        blocks.push({
            id: $b.attr('question_id'),
            name: name_box.val() || name_box.attr('placeholder'),
            dep: $b.find('select.dependency_list').val(),
        });
    })

    remove_cyclic(blocks, $question.attr('question_id'))

    $.each(blocks, (i, b) => {
        $select.append(new Option(b.name, b.id));
    });

    $select.val(val);
}

function show_question_list(checkbox) {
    const $question = $(checkbox.closest('div.question_box'));
    const $select = $question.find('select.dependency_list');
    const $condition = $question.find('input.dependency_condition');

    if (checkbox.checked) {
        generate_question_list($select);

        if (checkbox.hasAttribute('default_qid')) {
            $select.val(checkbox.getAttribute('default_qid'))
            $select.show();

            $condition.val(checkbox.getAttribute('default_condition'))
            $condition.show();

            checkbox.removeAttribute('default_condition');
            checkbox.removeAttribute('default_qid');
        } else {
            $select.fadeIn();
            $condition.fadeIn();
        }
    } else {
        $select.empty();
        $select.fadeOut();
        $condition.val('');
        $condition.fadeOut();
    }
    $select
        .prop('disabled', !checkbox.checked)
        .prop('readonly', !checkbox.checked);
    $condition
        .prop('disabled', !checkbox.checked)
        .prop('readonly', !checkbox.checked);
}

$(document).ready(() => {
    $('div.question_box input[name=is_dependent]').each((i, e) => show_question_list(e));
})

function update_dependency_selector_labels(nameInput) {
    const new_val = nameInput.value || nameInput.getAttribute('placeholder');
    const id = $(nameInput).closest('.question_box').attr('question_id')
    $(`select.dependency_list > option[value=${id}]`).text(new_val);
}

function update_dependency_on_remove(id) {
    $(`select.dependency_list`).each((i, e) => {
        if (e.value === id) {
            const $checkbox = $(e.parentNode).find('input[name=is_dependent]')
            $checkbox.get(0).checked = false;
            show_question_list($checkbox);
        }
    })

}