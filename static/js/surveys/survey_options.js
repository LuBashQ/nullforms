
function set_date(date_field){
    let id_field = date_field.id
    let minDate = get_current_date_ISO()
    if (!date_field.value) {
        //date_field.value = now.toISOString().split(/[a-z,A-Z]/)[0]
        $("#" + id_field + "").attr('min', minDate);
    }
    if (id_field.indexOf("end") !== -1)
        check_couple_date(date_field, "end", "start")
    if (id_field.indexOf("start") !== -1)
        check_couple_date(date_field, "start", "end")
}
function get_current_date_ISO(){
    let now = new Date()
    let currDate = now.toISOString().split(/[a-z,A-Z]/)[0]
    return currDate
}
function check_couple_date(curr_field, curr, other){
    let curr_field_id = curr_field.id
    let other_field_id = curr_field_id.replace(curr, other)
    let other_value = document.getElementById(other_field_id).value
    if(curr === "start"){
        let limitDate = other_value? other_value: 0
        $("#" + curr_field_id + "").attr({
            'min': get_current_date_ISO(),
            'max': limitDate
        });
    }
    else{
        let limitDate = other_value? other_value: 0
        $("#" + curr_field_id + "").attr('min', limitDate)
    }

}
function adjust_time(date_field){
    let time_field_id = date_field.id.replace("date", "time")
    let time_field = document.getElementById(time_field_id)
    set_time(time_field)
}

function set_time(time_field){
    let start_date_id = (time_field.id.substr(0, time_field.id.length-4)+"date").replace("end", "start")
    let end_date_id = (time_field.id.substr(0, time_field.id.length-4)+"date").replace("start", "end")
    let start_date_field = document.getElementById(start_date_id)
    let end_date_field = document.getElementById(end_date_id)
    if (time_field.id.indexOf("start") !== -1){
        let end_time_field_id = time_field.id.replace("start", "end")
        let end_time_field = document.getElementById(end_time_field_id)
        if(start_date_field.value === get_current_date_ISO()){
            $("#" + time_field.id + "").attr('min', get_current_time_ISO())
        }
        else{
            $("#" + time_field.id + "").removeAttr('min')
        }
        if(start_date_field.value === end_date_field.value && end_time_field.value){
            $("#" + time_field.id + "").attr('max', end_time_field.value)
        }
    }
    if(time_field.id.indexOf("end") !== -1){
        let start_time_field_id = time_field.id.replace("end", "start")
        let start_time_field = document.getElementById(start_time_field_id)
        if(end_date_field.value === get_current_date_ISO()){
            $("#" + time_field.id + "").attr('min', get_current_time_ISO())
        }
        else{
            $("#" + time_field.id + "").removeAttr('min')
        }
        if(start_date_field.value === end_date_field.value && start_time_field.value){
            $("#" + time_field.id + "").attr('min', start_time_field.value)
        }
    }


}
function get_current_time_ISO(){
    let now = new Date().toLocaleString()
    let curr_time = now.split(", ")[1].substr(0, 5)
    return curr_time
}

