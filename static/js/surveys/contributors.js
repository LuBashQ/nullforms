/**
 * Removes a contributor from the survey's contributors list by sending an HTTP DELETE request.
 * @param url the url of the deletion method
 * @param row the HTML table row element which contains the removed contributor's data
 * @return {Promise<void>}
 */
async function remove_contributor(url, row) {
    await fetch(url, {
        method: 'DELETE',
        body: new URLSearchParams()
    }).then(_ => row.remove())
        .catch(e => alert(e))
}

/**
 * Adds a contributor to the survey's contributors list by sending an HTTP POST request.
 * @param survey_id the id of the survey
 * @return {Promise<void>}
 */
async function add_contributor(survey_id) {
    let email = $('#contributor')
    let formData = new FormData()
    formData.append('email', email.val())

    await fetch(`/s/${survey_id}/add_contributor`, {
        method: 'POST',
        body: new URLSearchParams(formData),

    }).then(() => {
        window.location.reload()
    })
    .catch(e => alert(e))
}