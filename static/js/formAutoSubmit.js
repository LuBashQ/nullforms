// noinspection JSUnusedGlobalSymbols
$.widget("custom.formAutoSubmit", {
    // Default options.
    options: {
        timeout: 1000,
        preventUnloadOnDirty: true,
        // callbacks
        on_submission_fail: alert,
        beforeunload_message: "Are you sure you want to exit?",
    },

    _create: function () {
        this._domElement = this.element.get(0);

        if (this._domElement.nodeName !== 'FORM') throw Error('formAutoSubmit can only be used on forms, was used on ' + this._domElement.nodeName);

        this._lastSubmitted = this.element.serialize();
        this._before_unload_listener = function (event) {
            event.preventDefault();
            return event.returnValue = this.options.beforeunload_message;
        };

        this._on(this.element, {
            'click button': this._start_submit_timer,
            'keyup input,textarea': this._start_submit_timer,
            'change': this._start_submit_timer,
            'blur input': this._submit,
        });
    },

    _timeoutRef: null,
    _lastSubmitted: null,
    _domElement: null,
    _isDirty: false,
    _before_unload_listener: null,

    _start_submit_timer: function () {
        if (this._timeoutRef) clearTimeout(this._timeoutRef);

        this._timeoutRef = setTimeout(() => {
            this._submit();
        }, this.options.timeout);

        this.is_dirty(true);
    },

    _submit: function () {
        // clear timeout
        if (this._timeoutRef) clearTimeout(this._timeoutRef);
        this._timeoutRef = null;

        if (!this._domElement.isConnected // do not submit if element has been deleted
            || this.element.serialize() === this._lastSubmitted) { // or if content is the same as last submitted

            this.is_dirty(false);
            return;
        }

        const form = this._domElement;
        fetch(form.action, {
            method: form.method,
            body: new URLSearchParams([...(new FormData(form))])
        }).then(response => {
            // fail if response not ok
            if (!response.ok) throw Error(response.statusText);
            // update last submitted
            this._lastSubmitted = this.element.serialize();
            // remove alert before leaving the page
            this.is_dirty(false);
        }).catch(this.options.on_submission_fail);

    },

    is_dirty: function (value) {
        // No value passed, act as a getter.
        if (value === undefined) {
            return this._isDirty;
        }

        // Value passed, act as a setter.
        this._isDirty = value;

        // update events
        if (this.options.preventUnloadOnDirty) {
            if (this._isDirty) {
                addEventListener("beforeunload", this._before_unload_listener, {capture: true});
            } else {
                removeEventListener("beforeunload", this._before_unload_listener, {capture: true});
            }
        }
    },

})