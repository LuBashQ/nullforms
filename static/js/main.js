function open_tab(current, tab_name) {

    let tabs = document.getElementsByClassName('tabContent')
    for (let i = 0; i < tabs.length; i++) {
        tabs[i].style.display = 'none'
    }

    let links = document.getElementsByClassName("tabLink");
    for (let i = 0; i < links.length; i++) {
        links[i].className = links[i].className.replace(" active", "");
    }

    document.getElementById(tab_name).style.display = "block";
    if (current) current.className += " active";

    document.cookie = "activeTab=" + tab_name + "; SameSite=Lax";
}

function open_default_tab() {
    let oldActive = getCookie("activeTab")

    if (oldActive && document.getElementById(oldActive)) {
        open_tab(null, oldActive);
    } else {
        document.getElementById("defaultOpen").click();
    }
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}